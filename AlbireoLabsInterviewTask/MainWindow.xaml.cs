﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace AlbireoLabsInterviewTask
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Coefficient coefficient;
        public MainWindow()
        {
            this.DataContext = coefficient;
            this.coefficient = new Coefficient();
            InitializeComponent();
            Polyline myPolyline = new Polyline();
            myPolyline.Stroke = System.Windows.Media.Brushes.Yellow;
            myPolyline.StrokeThickness = 2;



            PointCollection myPointCollection = new PointCollection();

            for (int i = 0; i <= 300; i++)
            {
                Point p = new Point();
                p.X = i;
                p.Y = coefficient.CoefficientA * (Math.Pow(p.X, 2)) + p.X * 0.05 + 5;

                myPointCollection.Add(p);
            }

            myPolyline.Points = myPointCollection;
            myGrid.Children.Add(myPolyline);
        }

        //private double coefficient;
        //public double Coefficient
        //{
        //    get { return coefficient; }
        //    set
        //    {
        //        if (coefficient != value)
        //        {
        //            coefficient = value;
        //            OnPropertyChanged();
        //        }
        //    }

        //}


        //public event PropertyChangedEventHandler PropertyChanged;
        //private void OnPropertyChanged([CallerMemberName]string propertyName = null)
        //{
        //    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        //}
    }
}
